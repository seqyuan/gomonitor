# gomonitor

k8s docker 任务调度程序

# command
> gomonitor的模块

* **`taskmonitor`**       
    Add a task format list
* **`agsTask`**           
    Submit a depend list as workflow to argo/kubernetes
* **`submit`**            
    Submit a pod(ini conf) to argo/kubernetes
* **`psub`**               
    Submit a pod(command line) to kubernetes
* **`stat`**              
    List all project status monitored by this program
* **`retry`**         
    Retry a workflow/project
* **`rmproject`**         
    Remove a project, delete from the ~/.gomonitor.project.db
* **`cron`**               
    Do cron job

# 投递单个任务到k8s集群
gomonitor提供了1种方式投递job类型的任务到集群，2种方式投递单个pod到k8s集群
* submit
* psub

具体每个模块的用法参见wiki: https://gitlab.com/seqyuan/gomonitor/wikis/home

